import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  userVerify = true

  constructor(private userServ: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  public login(user: FormGroup) {
    let form = JSON.stringify(user.value);
    this.userServ.login(form).subscribe(
      response => {
        this.userVerify=true;
        console.log(response);
        localStorage.setItem('user', JSON.stringify(response));
        this.router.navigate(['/orders']);
      },
      err => {
        if(err.status == 404){
          this.userVerify = false
        }
      })

  }

}
