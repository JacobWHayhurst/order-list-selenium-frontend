import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = "http://localhost:9025/users/login";
  constructor(private httpCli: HttpClient) { }

  public login(loginForm): Observable<User>{
    const httpHead = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    })
  };

    return this.httpCli.post<User>(this.url, loginForm, httpHead);
  }
}
