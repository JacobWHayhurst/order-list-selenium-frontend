import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from './order';

@Injectable({
  providedIn: 'root'
})
export class OrderListService {

  private url = "http://localhost:9025/orders/rand";

  constructor(private httpCli: HttpClient) { }

  public getOrders(): Observable<Order[]>{
    const httpHead = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
    })
  };

    return this.httpCli.get<Order[]>(this.url, httpHead);
  }

}
