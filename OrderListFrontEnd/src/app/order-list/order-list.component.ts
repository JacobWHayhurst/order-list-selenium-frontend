import { Component, OnInit } from '@angular/core';
import { User } from '../login/user';
import { Order } from './order';
import { OrderListService } from './order-list.service';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {

  user: User;
  orderList: Order[];
  viewItem = false;
  itemCode: number;

  constructor(private orderServ: OrderListService) { }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('user'));
    console.log(this.user);
    this.orderServ.getOrders().subscribe(
      response => {
        console.log(response);
        this.orderList=response;
      
      })
  }

  public getMore(){
    this.orderServ.getOrders().subscribe(
      response => {
        console.log(response);
        this.orderList=response;
      })
  }

  public viewOrder(orderCode){
    this.viewItem = true;
    this.itemCode = orderCode;
  }

}
